#include "BoysAperySurface.h"

#include <vector>

#include <cmath>
#include <iostream>

const int BoysAperySurface::maxNumTriangles = 1000;
const int BoysAperySurface::minNumTriangles = 10;
const int BoysAperySurface::delta = 20;

void BoysAperySurface::updateGUI() {
    Application::updateGUI();

    ImGui::SetNextWindowPos(ImVec2(0, 0), ImGuiSetCond_FirstUseEver);
    if (ImGui::Begin("MIPT OpenGL Sample", NULL, ImGuiWindowFlags_AlwaysAutoResize)) {
        ImGui::Text("FPS %.1f", ImGui::GetIO().Framerate);
    }

    ImGui::End();
}

void BoysAperySurface::draw() {
    Application::draw();

    // Получаем текущие размеры экрана и выставлям вьюпорт
    int width, height;
    glfwGetFramebufferSize(_window, &width, &height);

    glViewport(0, 0, width, height);

    // Очищаем буферы цвета и глубины от результатов рендеринга предыдущего кадра
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // Подключаем шейдер
    assert(shader);
    shader->use();

    // Загружаем на видеокарту значения юниформ-переменные: время и матрицы
    shader->setFloatUniform("time", static_cast<float>(glfwGetTime())); //передаем время в шейдер

    shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
    shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

    // Загружаем на видеокарту матрицы модели мешей и запускаем отрисовку
    assert(surface);
    shader->setMat4Uniform("modelMatrix", surface->modelMatrix());
    surface->draw();
}

void BoysAperySurface::makeScene() {
    Application::makeScene();

    shader = std::make_shared<ShaderProgram>("495UpshinskiyData/shader.vert", "495UpshinskiyData/shader.frag");
    surface = createSurface(numTriangles);
}

void BoysAperySurface::update() {
    Application::update();

    if (needUpdate) {
        surface = createSurface(numTriangles);
        needUpdate = false;
    }
}

glm::vec3 BoysAperySurface::getPoint(float u, float v) {
    auto x = static_cast<float>((2. / 3.) * (cos(u) * cos(2 * v) +
                sqrt(2) * sin(u) * cos(v)) * cos(u) / (sqrt(2) - sin(2 * u) * sin(3 * v)));
    auto y = static_cast<float>((2. / 3.) * (cos(u) * sin(2 * v) -
                                             sqrt(2) * sin(u) * sin(v)) * cos(u) / (sqrt(2) - sin(2 * u) * sin(3 * v)));
    auto z = static_cast<float>(sqrt(2) * cos(u) * cos(u) / (sqrt(2) - sin(2 * u) * sin(2 * v)));

    return glm::vec3(x, y, z);
}

glm::vec3 BoysAperySurface::getNormal(float u, float v) {
    auto dx_du = static_cast<float>(
            (-2. * sin(u) * (cos(u) * cos(2 * v) + sqrt(2) * sin(u) * cos(v)) +
             2 * cos(u) * (sqrt(2) * cos(u) * cos(v) - sin(u) * cos(2 * v))) /
            (3 * (sqrt(2) - sin(2 * u) * sin(3 * v))) +
            (4 * cos(u) * cos(2 * u) * sin(3 * v) *
             (cos(u) * cos(2 * v) + sqrt(2) * sin(u) * cos(v))) /
            (3 * pow(sqrt(2) - sin(2 * u) * sin(3 * v), 2))
    );
    auto dx_dv = static_cast<float>(
            2 * sin(2 * u) * cos(u) * cos(3 * v) * (cos(u) * cos(2 * v) + sqrt(2) * sin(u) * cos(v)) /
            (pow(sqrt(2) - sin(2 * u) * sin(3 * v), 2)) +
            (2 * cos(u) * (-sqrt(2) * sin(u) * sin(v) - 2 * cos(u) * sin(2 * v))) /
            (3 * (sqrt(2) - sin(2 * u) * sin(3 * v)))
    );

    auto dy_du = static_cast<float>(
            (-2. * sin(u) * (cos(u) * sin(2 * v) + sqrt(2) * sin(u) * sin(v)) +
             2 * cos(u) * (sqrt(2) * cos(u) * sin(v) - sin(u) * sin(2 * v))) /
            (3 * (sqrt(2) - sin(2 * u) * sin(3 * v))) +
            (4 * cos(u) * cos(2 * u) * sin(3 * v) *
             (cos(u) * sin(2 * v) - sqrt(2) * sin(u) * sin(v))) /
            (3 * pow(sqrt(2) - sin(2 * u) * sin(3 * v), 2))
    );
    auto dy_dv = static_cast<float>(
            2 * sin(2 * u) * cos(u) * cos(3 * v) * (cos(u) * sin(2 * v) - sqrt(2) * sin(u) * sin(v)) /
            (pow(sqrt(2) - sin(2 * u) * sin(3 * v), 2)) +
            (2 * cos(u) * (-sqrt(2) * sin(u) * cos(v) + 2 * cos(u) * cos(2 * v))) /
            (3 * (sqrt(2) - sin(2 * u) * sin(3 * v)))
    );

    auto dz_du = static_cast<float>((4 * cos(u) * (-sin(u) + sqrt(2) * cos(u) * cos(v) * sin(v))) /
                                    (sqrt(2) - sin(2 * u) * pow(sin(2 * v), 2)));

    auto dz_dv = static_cast<float>(
            (2 * sqrt(2) * pow(cos(u), 2) * cos(2 * v) * sin(2 * u)) /
            (2. - 2. * sqrt(2) * sin(2 * u) * sin(2 * v) +
             pow(sin(2 * u), 2) * pow(sin(2 * v), 2))
    );

    glm::vec3 du(dx_du, dy_du, dz_du);
    glm::vec3 dv(dx_dv, dy_dv, dz_dv);

    glm::vec3 normal = glm::cross(du, dv);

    return glm::normalize(normal);
}

MeshPtr BoysAperySurface::createSurface(int n) {
    std::vector<glm::vec3> vertices;
    std::vector<glm::vec3> normals;

    float uMin = -glm::half_pi<float>(), uMax = glm::half_pi<float>();
    float vMin = 0, vMax = glm::pi<float>();

    for (int i = 0; i < n; ++i) {
        float u1 = uMin + (uMax - uMin) * i / n;
        float u2 = uMin + (uMax - uMin) * (i + 1) / n;

        for (int j = 0; j < n; ++j) {
            float v1 = vMin + (vMax - vMin) * j / n;
            float v2 = vMin + (vMax - vMin) * (j + 1) / n;

            vertices.push_back(getPoint(u1, v1));
            vertices.push_back(getPoint(u1, v2));
            vertices.push_back(getPoint(u2, v1));

            normals.push_back(getNormal(u1, v1));
            normals.push_back(getNormal(u1, v2));
            normals.push_back(getNormal(u2, v1));

            vertices.push_back(getPoint(u2, v2));
            vertices.push_back(getPoint(u2, v1));
            vertices.push_back(getPoint(u1, v2));

            normals.push_back(getNormal(u2, v2));
            normals.push_back(getNormal(u2, v2));
            normals.push_back(getNormal(u1, v2));
        }
    }

    auto buffer0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buffer0->setData(vertices.size() * sizeof(float) * 3, vertices.data());

    auto buffer1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buffer1->setData(normals.size() * sizeof(float) * 3, normals.data());

    auto mesh = std::make_shared<Mesh>();
    mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buffer0);
    mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buffer1);
    mesh->setPrimitiveType(GL_TRIANGLES);
    mesh->setVertexCount(vertices.size());

    mesh->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 0.5f)));

    return mesh;
}

void BoysAperySurface::handleKey(int key, int scancode, int action, int mods) {
    Application::handleKey(key, scancode, action, mods);

    if (action == GLFW_PRESS) {
        if (key == GLFW_KEY_EQUAL) {
            numTriangles += delta;
            numTriangles = std::min(numTriangles, maxNumTriangles);

            needUpdate = numTriangles != maxNumTriangles;
        } else if (key == GLFW_KEY_MINUS) {
            numTriangles -= delta;
            numTriangles = std::max(numTriangles, minNumTriangles);

            needUpdate = numTriangles != minNumTriangles;
        }
    }
}
