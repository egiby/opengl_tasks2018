#pragma once

#include <Application.hpp>
#include <Mesh.hpp>
#include <ShaderProgram.hpp>

class BoysAperySurface : public Application {
public:
    void makeScene() override;
    void update() override;
    void updateGUI() override;
    void draw() override;

    void handleKey(int key, int scancode, int action, int mods) override;
private:
    static glm::vec3 getPoint(float u, float v);
    static glm::vec3 getNormal(float u, float v);

    static MeshPtr createSurface(int n);

    MeshPtr surface;
    ShaderProgramPtr shader;

    int numTriangles = 100;
    bool needUpdate = false;

    static const int maxNumTriangles;
    static const int minNumTriangles;
    static const int delta;
};